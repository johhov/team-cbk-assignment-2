#pragma once

#if defined(__linux__)						// If we are using linux.
#include <GL/glew.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>
#include <gl\glew.h>
#endif

#include <iostream>

enum framebufferType
{
	NORMAL,
	SHADOWMAP,
	SHADOWMAP_CUBE
};

class Framebuffer
{
public:
	Framebuffer(int width, int height, framebufferType type);
	~Framebuffer();

	void bind();

	GLuint frameBufferObject, colorBuffer, depthBuffer;
};
