#include "TextureHandler.h"

GLuint TextureHandler::createTextureFromImage(const std::string& imageName)
{
	// SDL Image is used to load the image and save it as a surface.
	SDL_Surface* textureSurface = IMG_Load(imageName.c_str());
	if (!textureSurface)
	{
		printf("Failed to load texture \"%s\": %s\n", imageName.c_str(), IMG_GetError());
		return 0;
	}

	// We check if the texture has an alpha channel.
	GLenum textureFormat = GL_RGB;
	if (textureSurface->format->BytesPerPixel == 4)
	{
		textureFormat = GL_RGBA;
	}

	// Handles cases where the system stores the texel data differently.
	GLenum texelFormat = this->getCorrectTexelFormat(textureSurface);

	// We generate and bind the texture buffer. Telling opengl that any texture operations will be done on this buffer.
	this->textureBuffer = 0;
	glGenTextures(1, &textureBuffer);
	glBindTexture(GL_TEXTURE_2D, textureBuffer);

	// We tell OpenGL what to do when it needs to render the texture at a higher or lower resolution.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// We create the actual texture. We need to tell OpenGL about the various traits of the picture. Notice the data being passed in (textureSurface->pixels).
	glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, textureSurface->w, textureSurface->h, 0, texelFormat, GL_UNSIGNED_BYTE, textureSurface->pixels);

	// We unbind the texture by binding to zero.
	glBindTexture(GL_TEXTURE_2D, 0);

	// It's important to always free memory when you are done with it.
	SDL_FreeSurface(textureSurface);

	return textureBuffer;
}

GLuint TextureHandler::createTexture(const GLsizei textureWidth, const GLsizei textureHeight, const GLint filtering)
{
	GLuint texture = 0;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filtering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	return texture;
}

GLenum TextureHandler::getCorrectTexelFormat(const SDL_Surface *textureSurface)
{
	/*
	*  SDL_image will load images in the BGR(A) format on some systems.
	*  Here we check if the blue color is the first color and sets the BGR(A) format if it is.
	*/

	//Finds the right format of the loaded picture
	GLenum texelFormat = GL_RGB;
	if (textureSurface->format->Bmask == 0xFF)
	{
		texelFormat = GL_BGR;
		if (textureSurface->format->BytesPerPixel == 4)
		{
			texelFormat = GL_BGRA;
		}
	}
	else
	{
		if (textureSurface->format->BytesPerPixel == 4)
		{
			texelFormat = GL_RGBA;
		}
	}

	return texelFormat;
}