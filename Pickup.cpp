#include "Pickup.h"

Pickup::Pickup()
{
}

Pickup::~Pickup()
{
}

bool Pickup::update(const glm::vec3& playerPos, const float& deltaTime)
{
	glm::vec3 diff = playerPos - this->position;

	//if closer then 1 to pacman
	//Register as hit
	if (glm::length(diff) < 1)
	{
		return true;
	}

	return false;
}