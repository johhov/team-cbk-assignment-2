#pragma once

#if defined(__linux__)						// If we are using linux.
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <glm/gtc/type_ptr.hpp>
#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <assimp\Importer.hpp>
#include <glm/gtc/type_ptr.hpp>
#endif

#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

#include "Camera.h"
#include "ShaderHandler.h"
#include "LightHandler.h"

struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
};

struct Texture
{
	GLuint id;
	std::string type;
	aiString path;
};

class Mesh
{
public:
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures);
	void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler, glm::mat4* modelMatrix);	//need to get shaderProgram from Shaderhandler ;)

private:
	//--------------------------------PRIVATE VARIABLES----------------------------------
	GLuint VAO, VBO, EBO;

	//-------------------------------PRIVATE FUNCTIONS-----------------------------------
	void setupMesh();
};

//lots of inspiration from http://learnopengl.com/#!Model-Loading/Assimp