#pragma warning( push )
#pragma warning( disable : 4996)

#include "WindowHandler.h"

#include <stdio.h>
#include <SDL2/SDL_image.h>

#include "globals.h"

bool WindowHandler::init()
{
	loadConfig();

	if (!initSDL())
	{
		return false;
	}

	return true;
}

void WindowHandler::loadConfig()
{
	FILE* conf = fopen(gWINDOW_CONFIGURATION_FILE, "r");

	fscanf(conf, "Window name: %[^\n]%*c", windowName);
	fscanf(conf, "Window size: %dx%d", &windowXSize, &windowYSize);

	fclose(conf);
	conf = nullptr;
}

bool WindowHandler::initSDL()
{
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Ask SDL to give us a core context.
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		//Create window
		window = SDL_CreateWindow(
			windowName,
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			windowXSize,
			windowYSize,
			SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN
		);

		if (window == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			context = SDL_GL_CreateContext(window);

			if (context == NULL)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}
			}
		}
	}
	return success;
}

void WindowHandler::close()
{
	SDL_DestroyWindow(window);
	window = nullptr;

	IMG_Quit();
	SDL_Quit();
}

//---------------------------------------------------GET------------------------------------------
glm::vec2 WindowHandler::getScreenSize()
{
	glm::vec2 size;
	size.x = windowXSize;
	size.y = windowYSize;
	return size;
}

SDL_Window* WindowHandler::getWindow()
{
	return window;
}

#pragma warning(pop)