#version 330 core

struct Light 
{
    vec3 position;  
  
    vec3 diffuse;
    vec3 specular;
	
    float constant;
    float linear;
    float quadratic;
}; 

out vec4 color;

//Recieves info from vertex shader
in VS_OUT
{
	in vec3 Normal;
	in vec3 FragPos;
	in vec2 TextCoords;
} fs_in;

//Used to make shadows
uniform samplerCube depthMap;
uniform float farPlane;

uniform Light light;
uniform vec3 cameraPosition;

//Normal texture
uniform sampler2D textureDiffuse1;
uniform sampler2D textureSpecular1;

//Calculates if the fragment is in shadow or not
float shadowCalculation(vec3 fragPos)
{
	vec3 fragToLight = fragPos - light.position;
	//Samples depth value
	float closestDepth = texture(depthMap, fragToLight).r;
	
	//Takes away 0-1 range normalization
	closestDepth *= farPlane;
	
	//Depth between fragment and light
	float currentDepth = length(fragToLight);
	
	//Adds a bias to shadows to avoid shadow acne. Bias is 0 when fragment normal points straight at the light
	//And largest when it points 90 degrees away
	float bias = max(0.35 * (1.0 - dot(fs_in.Normal, normalize(fragToLight))), 0.005);
	
	//Returns 1 if not in shadow and 0 if in shadow
	float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;
	
	return shadow;
}
	
void main()
{
	//variables
	vec3 normal = normalize(fs_in.Normal);
    vec3 viewDir = normalize(cameraPosition - fs_in.FragPos);
	vec3 lightDir = normalize(light.position - fs_in.FragPos);
	
	float diff = max(dot(normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 1);    
	
	//Samples texture for fragment color
	vec3 textureColor = texture(textureDiffuse1, fs_in.TextCoords).xyz;
	
	//Ambient, diffused and specular values
	vec3 ambient = 0.3 * textureColor;
    vec3 diffuse = light.diffuse * diff * textureColor;
    vec3 specular = light.specular * spec * texture(textureSpecular1, fs_in.TextCoords).xyz;

    // Attenuation
    float distance = length(light.position - fs_in.FragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance)); 
    
    // Combine results
    diffuse *= attenuation;
    specular *= attenuation;
	
	//if in shadow or not
	float shadow = shadowCalculation(fs_in.FragPos);
	
	//Color of fragment
	color = vec4(ambient + (1.0 - shadow) * (diffuse + specular) * textureColor, 1.0);
}

